## Getting Started

<!-- Setup -->
Nodejs version >= 12

step 1: npm install
step 2: npm run build 
step 3: npm run generate 

<!-- ------------- -->
First, run the development server:

```bash
npm run dev

```

## Setting ESlint + Prettier

1. In VS Code Setup, install ESLint + Prettier
2. First try (⌘Cmd + , or Ctrl + ,) → type in settings.json file → click on Edit in settings.json. If that doesn't work the file is located in $/Code/User/settings.json

```bash
  // Run formatter when you save code changes
  "editor.formatOnSave": true,
  // Disable default formatting (ESLint formatting will be used instead)
  "[javascript]": {
      "editor.formatOnSave": true,
      "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  // Auto-fix issues with ESLint when you save code changes
  "editor.codeActionsOnSave": {
      "source.fixAll.eslint": true
  },

```

## VScode

Recommend install:

1. Vetur
2. Bracket Pair Colorizer
3. Auto Rename Tag
4. Auto Close Tag