import { Plugin } from '@nuxt/types';
import { NuxtAxiosInstance } from '@nuxtjs/axios';

declare module '@nuxt/types' {
  interface Context {
    $fetchApi: NuxtAxiosInstance;
  }
}
const fetchApiPlugin: Plugin = ({ $axios }, inject) => {
  const axiosCustom = $axios.create({
    headers: {
      common: {
        Accept: 'application/json, text/plain, */*',
      },
    },
  });
  axiosCustom.setToken('');

  axiosCustom.onRequest((config) => {
    // console.log('🚀 Making request to ' + config.url);
  });

  axiosCustom.interceptors.response.use(
    (res) => ({ response: res.data }),
    (err) => {
      const response = err.response || {};
      const responseError = response.data || {};
      if (responseError.message) {
        responseError.status = response.status;
        throw responseError;
      }
      const error: any = new Error(
        responseError.message || `Đã có lỗi xảy ra, xin hãy thử lại sau (${response.status || `500`})`
      );
      error.status = response.status || 500;
      throw error;
    }
  );

  inject('fetchApi', axiosCustom);
};

export default fetchApiPlugin;
