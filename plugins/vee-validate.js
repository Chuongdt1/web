import Vue from 'vue';
import { extend, ValidationObserver, ValidationProvider } from 'vee-validate';
import { required, email } from 'vee-validate/dist/rules';

extend('required', {
  ...required,
  message: () => {
    return parent.$nuxt.$t('business_contact.form.required');
  },
});

extend('email', {
  ...email,
  message: () => {
    return parent.$nuxt.$t('business_contact.form.format_email');
  },
});

// Register it globally
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

export default (context, inject) => {
  inject('validator', ValidationProvider);
};
