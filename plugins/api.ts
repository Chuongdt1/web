import { Plugin } from '@nuxt/types';
import Category from '@/api/category';
declare module '@nuxt/types' {
  interface Context {
    $api: {
      category: CategoryApi;
    };
  }
}

declare module 'vue/types/vue' {
  interface Vue {
    $api: {
      category: CategoryApi;
    };
  }
}

declare module 'vuex/types/index' {
  interface Store<S> {
    $api: {
      category: CategoryApi;
    };
  }
}

const apiPlugin: Plugin = ({ $fetchApi }, inject) => {
  const factories = {
    category: Category($fetchApi),
  };

  inject('api', factories);
};

export default apiPlugin;
