import { DEVELOPMENT, PRODUCTION, SANDBOX, STAGING } from './constants/enviroment';

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: true,
  // Target: https://go.nuxtjs.dev/config-target
  target: 'server',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head() {
    const i18nHead = this.$nuxtI18nHead({ addSeoAttributes: true });
    return {
      htmlAttrs: {
        myAttribute: '9Pay.vn',
        ...i18nHead.htmlAttrs,
      },
      meta: [
        { charset: 'utf-8' },
        {
          httpEquiv: 'ScreenOrientation',
          content: 'autoRotate:disabled',
        },
        {
          name: 'viewport',
          content: 'width=device-width, initial-scale=1 maximum-scale=1',
        },
        { hid: 'description', name: 'description', content: '' },
        { name: 'format-detection', content: 'telephone=no' },
      ],
      link: [
        {
          rel: 'icon',
          type: 'image/x-icon',
          href: 'https://storage.googleapis.com/npay/assets/static/landing/favicon.ico',
        },
        // ...i18nHead.link,
      ],
      script: [
        {
          src: 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js',
        },
      ],
    };
  },

  styleResources: {
    scss: ['~/assets/styles/abstracts/index.scss'],
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // '~/assets/styles/reset.scss',
    '~/assets/styles/vendors/bootstrap.scss',
    '~/assets/styles/base/index.scss',
    '~/assets/styles/components/index.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/fetchApi' },
    { src: '~/plugins/api' },
    { src: '~/plugins/vee-validate.js', mode: 'client' },
    { src: '~/plugins/vue-number-animation.js', mode: 'client' },
    { src: '~/plugins/vue-slick-carousel.js', mode: 'client' },
    { src: '~/plugins/lottie.js', mode: 'client' },
    { src: '~/plugins/vue-toast.js', mode: 'client' },
  ],

  purgeCSS: {
    whitelist: ['aos-init', 'aos-animate', 'data-aos-delay', 'data-aos-duration', 'fade-up', 'zoom-in'],
  },

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [{ path: '~/components/common', extensions: ['vue'] }],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ['@nuxtjs/dotenv', '@nuxt/typescript-build', '@nuxtjs/google-analytics', '@nuxt/image'],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ['@nuxtjs/axios', '@nuxtjs/i18n', '@nuxtjs/style-resources'],

  i18n: {
    baseUrl: 'https://9pay.vn',
    locales: [
      {
        code: 'en',
        iso: 'en-US',
        name: 'English',
      },
      {
        code: 'vi',
        iso: 'vi-VN',
        name: 'Tiếng Việt',
      },
    ],
    parsePages: false,
    defaultLocale: 'vi',
    detectBrowserLanguage: false,
    vueI18n: {
      fallbackLocale: 'vi',
      messages: {
        en: require('./locales/en-US.json'),
        vi: require('./locales/vi-VN.json'),
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    // transpile: ['vue-lodash'],
    // publicPath: '/v1/',
    extractCSS: true,
    postcss: false,
    terser: {
      // https://github.com/terser/terser#compress-options
      terserOptions: {
        compress: {
          drop_console: [DEVELOPMENT, STAGING].includes(process.env.APP_ENV) ? false : true,
        },
      },
    },
  },
  vue: {
    config: {
      devtools: [DEVELOPMENT, STAGING].includes(process.env.APP_ENV) ? true : false,
    },
  },
  generate: {
    dir: 'dist',
  },
  router: {
    base: '/',
    middleware: ['categories'],
  },
  render: {
    static: {
      prefix: true,
    },
  },
  server: {
    port: process.env.PORT || 3000, // default: 3001
    // host: '0.0.0.0', // default: localhost,
    // timing: false,
  },
  env: {
    // baseUrl: 'https://beta-be-wallet.9pay.mobi',
    baseUrl: process.env.API_URL,
  },

  googleAnalytics: {
    id: process.env.GOOGLE_ANALYTICS_ID, // Use as fallback if no runtime config is provided
  },
  publicRuntimeConfig: {
    googleAnalytics: {
      id: process.env.GOOGLE_ANALYTICS_ID,
    },
  },
  typescript: {
    typeCheck: true,
  },
  loading: {
    color: '#15ae62',
    height: '3px',
  },
};
