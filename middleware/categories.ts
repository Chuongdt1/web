import { Middleware } from '@nuxt/types';
import get from 'lodash/get';

const myMiddleware: Middleware = async (context) => {
  const { store } = context;
  const isCategoryNew = get(store, 'state.categories.categoryNew.length', 0);
  if (isCategoryNew) {
    return;
  }
  await Promise.all([store.dispatch('categories/getCategoryNew')]);
};

export default myMiddleware;
