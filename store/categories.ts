import { ActionTree, MutationTree } from 'vuex';
export type RootState = ReturnType<typeof state>;

const initialState = {
  categoryNew: [],
  isLoading: false,
  errorCategoryNew: {},
};

export const state = () => ({
  ...initialState,
});

export const actions: ActionTree<RootState, RootState> = {
  async getCategoryNew({ commit, state }) {
    try {
      if (state.categoryNew.length) {
        return false;
      }
      commit('FETCHING_CATEGORY_NEW', true);

      const { response } = await this.$api.category.getListCategory(1);
      commit('FETCHING_CATEGORY_NEW_SUCCESS', response.data);
      return response;
    } catch (error: any) {
      const err = {
        message: error.message,
        status: error?.status,
      };
      commit('FETCHING_CATEGORY_NEW_FAILURE', err);
      const errorMessage = error.message;
      return { errorMessage };
    }
  },
};

export const mutations: MutationTree<RootState> = {
  // Category news
  FETCHING_CATEGORY_NEW(state, data) {
    state.isLoading = data;
  },
  FETCHING_CATEGORY_NEW_SUCCESS(state, data) {
    state.categoryNew = data;
    state.isLoading = false;
  },
  FETCHING_CATEGORY_NEW_FAILURE(state, data) {
    state.errorCategoryNew = data;
    state.isLoading = false;
  },
};
