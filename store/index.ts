import { ActionTree, MutationTree } from 'vuex';
export type RootState = ReturnType<typeof state>;

export const state = () => ({
  config: {
    host: null,
  },
});

export const actions: ActionTree<RootState, RootState> = {
  nuxtServerInit(context, { req }) {
    const hostName = req.headers.host;
    console.log('🚀 server init ::>', `${hostName}`);
    context.commit('SET_HOST_NAME', hostName);
  },
};

export const mutations: MutationTree<RootState> = {
  SET_HOST_NAME(state, data) {
    state.config.host = data;
  },
};
