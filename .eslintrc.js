module.exports = {
  root: true,
  env: {
    node: true,
    browser: true,
    es6: true,
  },
  // parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
  },
  extends: [
    // '@nuxtjs/eslint-config-typescript'
    // 'plugin:vue/recommended',
    'eslint:recommended',
    '@vue/eslint-config-typescript/recommended',
    'plugin:prettier/recommended',
    // 'prettier/vue',
  ],
  rules: {
    'vue/attribute-hyphenation': 'off',
    // 'vue/max-attributes-per-line': [
    //   'error',
    //   {
    //     singleline: {
    //       max: 1,
    //     },
    //     multiline: {
    //       max: 1,
    //     },
    //   },
    // ],
    // "vue/component-name-in-template-casing": ["error", "PascalCase"],
    // 'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'error',
    // 'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'error',
    // // "indent": ["error", 2],
    // "no-undef": "error",
    // "no-use-before-define": "error",
  },
  globals: {
    $nuxt: true,
  },
};
