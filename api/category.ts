import { NuxtAxiosInstance } from '@nuxtjs/axios';
const API_URL = process.env.baseUrl;

export default (fetchApi: NuxtAxiosInstance): CategoryApi => ({
  getListCategory(type: any) {
    return fetchApi(API_URL + '/web-9pay/news-category?type=' + type + '&parent_id=0&category_web=web', {
      method: 'get',
    });
  },
});
