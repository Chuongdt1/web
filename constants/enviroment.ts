export const DEVELOPMENT = 'development';
export const STAGING = 'staging';
export const BETA = 'beta';
export const PRODUCTION = 'production';
export const SANDBOX = 'sandbox';
